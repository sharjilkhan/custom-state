<?php
namespace Custom\State\Model\Plugin\Shipping\Rate\Result;

class Append
{
    /**
     * @var \Magento\Checkout\Model\Session|\Magento\Backend\Model\Session\Quote
     */
    protected $session;

    /**
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Backend\Model\Session\Quote $backendQuoteSession
     * @param \Magento\Framework\App\State $state
     * @internal param Session $session
     */
    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Backend\Model\Session\Quote $backendQuoteSession,
        \Magento\Framework\App\State $state
    ) {
        if ($state->getAreaCode() == \Magento\Framework\App\Area::AREA_ADMINHTML) {
            $this->session = $backendQuoteSession;
        } else {
            $this->session = $checkoutSession;
        }
    }

    /**
     * Validate each shipping method before append.
     * Apply the rules action if validation was successful.
     * Can mark some rules as disabled. The disabled rules will be removed in the class
     * @see MageWorx\ShippingRules\Model\Plugin\Shipping\Rate\Result\GetAllRates
     * by checking the value of this mark in the rate object.
     *
     * NOTE: If you have some problems with the rules and the shipping methods, start debugging from here.
     *
     * @param \Magento\Shipping\Model\Rate\Result $subject
     * @param \Magento\Quote\Model\Quote\Address\RateResult\AbstractResult|\Magento\Shipping\Model\Rate\Result $result
     * @return array
     */
    public function beforeAppend($subject, $result)
    {
        if (!$result instanceof \Magento\Quote\Model\Quote\Address\RateResult\Method) {
            return [$result];
        }

        $filtableMethods = [
            'flatrate_flatrate',
            'ups_XDM',
            'ups_XPR',
            'ups_WXS',
            'carrier_method',
            'freeshipping_freeshipping'
            // ... add here your method codes
        ];
        $methodCode = $result->getCarrier() . '_' . $result->getMethod();
        //echo $methodCode;die;
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/mylog.log');
		$logger = new \Zend\Log\Logger();
		$logger->addWriter($writer);
		$logger->info($methodCode);
        if (!in_array($methodCode, $filtableMethods)) {
            return [$result];
        }

        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $this->session->getQuote();
        $quoteItems = $quote->getAllItems();
        $heavyWeightFlag = false;
        $weight = 0;
        foreach ($quoteItems as $item) {
            $weight += ($item->getWeight() * $item->getQty());
        }
        if($weight >= 40)
        {
            $logger->info('true');
            $heavyWeightFlag = true;
        }
        $logger->info('cart weight:');
        $logger->info($weight);
        if ($heavyWeightFlag == true) {
            $result->setIsDisabled(true);
        }

        return [$result];
    }
}
?>