<?php

namespace Custom\State\Setup;

use \Magento\Framework\Setup\InstallDataInterface;
use \Magento\Framework\Setup\ModuleDataSetupInterface;
use \Magento\Framework\Setup\ModuleContextInterface;
use \Magento\Framework\DB\Ddl\Table;

class InstallData implements InstallDataInterface{

    public function __construct(
        \Magento\Framework\Module\Dir\Reader $moduleReader,
        \Magento\Framework\File\Csv $fileCsv 
    ){
        $this->_moduleReader =$moduleReader;
        $this->_fileCsv =$fileCsv;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context){

        $setup->startSetup();
        $data = $this->getCSV();

        $num = count($data);
        echo "<pre>";
            for ($c=1; $c < $num; $c++) {
                $data1 = $data[$c];  
                $code = $data1[0];
                $StateName = $data1[1];
                $setup->getConnection()->Insert(
                    $setup->getTable('directory_country_region'),
                    [
                        'country_id' => 'AE',
                        'code' => $code,
                        'default_name' => $StateName
                    ]
                );
            }

        $setup->endSetup();
    }

    public function getCSV(){
        $directory = $this->_moduleReader->getModuleDir('etc', 'Custom_State');
        $file = $directory . '/book1.csv';
        if (file_exists($file)) {
        $data = $this->_fileCsv->getData($file);
        return $data;
        }
    }
}